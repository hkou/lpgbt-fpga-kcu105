##===================================================================================================##
##========================================  I/O PINS  ===============================================##
##===================================================================================================##
##=======##
## RESET ##
##=======##
set_property PACKAGE_PIN AN8 [get_ports CPU_RESET]
set_property IOSTANDARD LVCMOS18 [get_ports CPU_RESET]

##==========##
## MGT(GTX) ##
##==========##

## SERIAL LANES:
##--------------

# Use of SMAs
set_property PACKAGE_PIN R4 [get_ports SFP0_TX_P]
set_property PACKAGE_PIN R3 [get_ports SFP0_TX_N]
set_property PACKAGE_PIN P2 [get_ports SFP0_RX_P]
set_property PACKAGE_PIN P1 [get_ports SFP0_RX_N]

# Use of SFP
#set_property PACKAGE_PIN U4 [get_ports SFP0_TX_P]
#set_property PACKAGE_PIN U3 [get_ports SFP0_TX_N]
#set_property PACKAGE_PIN T2 [get_ports SFP0_RX_P]
#set_property PACKAGE_PIN T1 [get_ports SFP0_RX_N]

## SFP CONTROL:
##-------------
set_property PACKAGE_PIN AL8 [get_ports SFP0_TX_DISABLE]
set_property IOSTANDARD LVCMOS18 [get_ports SFP0_TX_DISABLE]

##====================##
## SIGNALS FORWARDING ##
##====================##

## SMA OUTPUT:
##------------
set_property PACKAGE_PIN H27 [get_ports USER_SMA_GPIO_P]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_P]
set_property SLEW FAST [get_ports USER_SMA_GPIO_P]

set_property PACKAGE_PIN G27 [get_ports USER_SMA_GPIO_N]
set_property IOSTANDARD LVCMOS18 [get_ports USER_SMA_GPIO_N]
set_property SLEW FAST [get_ports USER_SMA_GPIO_N]